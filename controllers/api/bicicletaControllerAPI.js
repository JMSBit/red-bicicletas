const { allBicis, add } = require("../../models/bicicleta");
var Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = function (req, res) {
  Bicicleta.allBicis((err, result) => {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json({
        bicicletas: result,
      });
    }
  });
};

exports.bicicleta_create = function (req, res) {
  bici = {
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.lat, req.body.lng],
  };
  Bicicleta.add(bici, (err) => {
    if (err) {
      console.log(`Error en add bici: ${err}`);
    } else {
      res.status(200).json({
        bicicleta: bici,
      });
    }
  });
};

exports.bicicleta_delete = function (req, res) {
  Bicicleta.removeByCode(req.body.code, (err) => {
    if (err) {
      console.log(`Error en delete bici: ${err}`);
    } else {
      res.status(204).send();
    }
  });
};

exports.bicicleta_update = function (req, res) {
  Bicicleta.findByCode(req.body.code, (err, bici) => {
    if (err) {
      console.log(`Error en update bici: ${err}`);
    } else if (bici) {
      bici.color = req.body.color;
      bici.modelo = req.body.modelo;
      bici.ubicacion = [req.body.lat, req.body.lng];
      Bicicleta.updateOne({ code: req.body.code }, { $set: bici }, (err) => {
        if (err) {
          console.log(`Error en update bici: ${err}`);
        } else {
          res.status(200).json({
            bicicleta: bici,
          });
        }
      });
    } else {
      res.status(200).json({
        bicicleta: {},
      });
    }
  });
};