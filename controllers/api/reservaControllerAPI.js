var Reserva = require("../../models/reserva");

exports.reserva_list = function (req, res) {
    Reserva.find({}, function (err, reservas) {
        res.status(200).json({
            reservas: reservas,
        });
    });
};

exports.reserva_create = function (req, res) {
    var reserva = new Reserva({
        desde: req.body.desde,
        hasta: req.body.hasta,
        bicicleta: req.body.bicicleta,
        usuario: req.body.usuario,
    });

    reserva.save(function (err) {
        res.status(200).json(reserva);
    });
};

exports.reserva_update = function (req, res) {
    Reserva.findById(req.body.usuario, (err, reserva) => {
        if (err) {
            console.log(`Error en update bici: ${err}`);
        } else if (reserva) {
            reserva.desde = req.body.desde;
            reserva.hasta = req.body.hasta;
            reserva.bicicleta = req.body.bicicleta;
            reserva.usuario = req.body.usuario;
            Reserva.updateOne({
                usuario: req.body.usuario
            }, {
                $set: reserva
            }, (err) => {
                if (err) {
                    console.log(`Error en update bici: ${err}`);
                } else {
                    res.status(200).json({
                        reserva: reserva,
                    });
                }
            });
        } else {
            res.status(200).json({
                reserva: {},
            });
        }
    });
};

exports.reserva_delete = function (req, res) {
    Reserva.removeById(req.body.usuario, (err) => {
        if (err) {
            console.log(`Error en delete reserva: ${err}`);
        } else {
            res.status(204).send();
        }
    });
};