var Usuario = require("../../models/usuario");

exports.usuarios_list = function (req, res) {
    Usuario.find({}, function (err, usuarios) {
        res.status(200).json({
            usuarios: usuarios,
        });
    });
};

exports.usuarios_create = function (req, res) {
    var usuario = new Usuario({
        nombre: req.body.nombre
    });

    usuario.save(function (err) {
        res.status(200).json(usuario);
    });
};

exports.usuarios_reservar = function (req, res) {
    Usuario.findById(req.body.usuario, function (err, usuario) {
        usuario.reservar(
            req.body.usuario,
            req.body.bicicleta,
            req.body.desde,
            req.body.hasta,
            function (err) {
                res.status(200).send();
            }
        );
    });
};