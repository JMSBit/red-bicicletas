var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;

var usuarioSchema = new Schema({
    nombre: String,
});

usuarioSchema.methods.reservar = function (usuario, bicicleta, desde, hasta, cb) {
    var reserva = new Reserva({
        usuario: usuario ? usuario : this._id,
        bicicleta,
        desde,
        hasta,
    });
    reserva.save(cb);
};

module.exports = mongoose.model('Usuario', usuarioSchema);