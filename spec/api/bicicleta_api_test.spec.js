var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");

var base_url = "http://localhost:5000/api/bicicletas";

describe("Bicicleta API", () => {
  // beforeEach(function (done) {
  //   var mongoDB = "mongodb://localhost/testdb";

  //   mongoose.connect(mongoDB, {
  //     useNewUrlParser: true,
  //     useUnifiedTopology: true,
  //     useCreateIndex: true,
  //   });

  //   const db = mongoose.connection;
  //   db.on("error", console.error.bind(console, "connection error"));
  //   db.once("open", function () {
  //     console.log("We are connected to test database");
  //     done();
  //   });
  // });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("GET BICICLETAS /", () => {
    it("Status 200", (done) => {
      request.get(base_url, function (error, response, body) {
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe("POST BICICLETAS /create", () => {
    it("Status 200", (done) => {
      var headers = { "content-type": "application/json" };
      var aBici =
        '{"code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 }';
      request.post(
        {
          headers: headers,
          url: `${base_url}/create`,
          body: aBici,
        },
        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          var bici = JSON.parse(body).bicicleta;
          expect(bici.color).toBe("rojo");
          expect(bici.ubicacion[0]).toBe(-34);
          expect(bici.ubicacion[1]).toBe(-54);
          done();
        }
      );
    });
  });

  describe("POST BICICLETAS /update", () => {
    it("Status 200", (done) => {
      var headers = { "content-type": "application/json" };
      var b = Bicicleta.createInstance(2, "negro", "urbana", [
        -34.860625,
        -56.206153,
      ]);
      Bicicleta.add(b, function (err, newBici) {
        request.post(
          {
            headers: headers,
            url: `${base_url}/update`,
            body:
              '{"code": 2, "color": "azul", "modelo": "urbana", "lat": -34.860625, "lng": -56.206153 }',
          },
          function (error, response, body) {
            expect(response.statusCode).toBe(200);
            Bicicleta.findByCode(2, (err, bici) => {
              if (err) console.log({ err });
              expect(bici.color).toBe("azul");
              done();
            });
          }
        );
      });
    });
  });

  describe("DELETE BICICLETAS /delete", () => {
    it("Status 204", (done) => {
      var a = Bicicleta.createInstance(1, "negro", "urbana", [
        -34.860625,
        -56.206153,
      ]);
      Bicicleta.add(a, function (err, newBici) {
        var headers = { "content-type": "application/json" };
        var aBici =
          '{"code": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54 }';

        Bicicleta.add(aBici, function (err, newBici) {
          request.delete(
            {
              headers: headers,
              url: `${base_url}/delete`,
              body: '{"code" : 10}',
            },
            function (error, response, body) {
              expect(response.statusCode).toBe(204);
              expect(Bicicleta.allBicis.length).toBe(1);
              done();
            }
          );
        });
      });
    });
  });
});