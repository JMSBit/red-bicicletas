var mongoose = require("mongoose");
var Reserva = require("../../models/reserva");
var Usuario = require("../../models/usuario");
var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");

var base_url = "http://localhost:5000/api/usuarios";

describe("Usuario API", () => {
  afterEach(function (done) {
    Usuario.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      Bicicleta.deleteMany({}, function (err, success) {
        if (err) console.log(err);
        Reserva.deleteMany({}, function (err, success) {
          if (err) console.log(err);
          //   mongoose.disconnect(err);
          done();
        });
      });
    });
  });

  describe("GET USUARIOS /", () => {
    it("Status 200", (done) => {
      request.get(base_url, function (error, response, body) {
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.usuarios.length).toBe(0);
        done();
      });
    });
  });

  describe("POST USUARIOS /create", () => {
    it("Status 200", (done) => {
      var headers = {
        "content-type": "application/json"
      };

      const usuario = {
        nombre: "Jonathan"
      };
      request.post({
          headers: headers,
          url: `${base_url}/create`,
          body: JSON.stringify(usuario),
        },

        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          var usuarios = JSON.parse(body);
          expect(usuarios.nombre).toBe("Jonathan");
          done();
        }
      );
    });
  });

  describe("POST USUARIO /reservar", () => {
    it("Status 200", (done) => {
      var headers = {
        "content-type": "application/json"
      };

      const u = new Usuario({
        nombre: "Jonathan"
      });
      u.save();

      const b = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urbana",
      });
      b.save();

      var desde = new Date(2020, 10 - 1, 18).toISOString();
      var hasta = new Date(2020, 10 - 1, 20).toISOString();

      var contentU = {
        usuario: String(u.id),
        bicicleta: String(b._id),
        desde,
        hasta,
      };
      request.post({
          headers: headers,
          url: `${base_url}/reservar`,
          body: JSON.stringify(contentU),
        },

        function (error, response, body) {
          expect(response.statusCode).toBe(200);
          Reserva.find({})
            .exec(function (err, reservas) {
              expect(reservas.length).toBe(1);
              expect(String(reservas[0].usuario._id)).toBe(contentU.usuario);
              done();
            });
        }
      );
    });
  });
});