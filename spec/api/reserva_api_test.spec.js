var mongoose = require("mongoose");
var Reserva = require("../../models/reserva");
var Usuario = require("../../models/usuario");
var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");

var base_url = "http://localhost:5000/api/reservas";

describe("Bicicleta API", () => {
    // beforeEach(function (done) {
    //   var mongoDB = "mongodb://localhost/testdb";

    //   mongoose.connect(mongoDB, {
    //     useNewUrlParser: true,
    //     useUnifiedTopology: true,
    //     useCreateIndex: true,
    //   });

    //   const db = mongoose.connection;
    //   db.on("error", console.error.bind(console, "connection error"));
    //   db.once("open", function () {
    //     console.log("We are connected to test database");
    //     done();
    //   });
    // });

    afterEach(function (done) {
        Usuario.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            Bicicleta.deleteMany({}, function (err, success) {
                if (err) console.log(err);
                Reserva.deleteMany({}, function (err, success) {
                    if (err) console.log(err);
                    //   mongoose.disconnect(err);
                    done();
                });
            });
        });
    });

    describe("GET RESERVAS /", () => {
        it("Status 200", (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.reservas.length).toBe(0);
                done();
            });
        });
    });

    describe("POST RESERVAS /create", () => {
        it("Status 200", (done) => {
            var headers = {
                "content-type": "application/json"
            };

            const usuario = new Usuario({
                nombre: "Jonathan"
            });
            usuario.save();

            const bicicleta = new Bicicleta({
                code: 1,
                color: "verde",
                modelo: "urbana",
            });
            bicicleta.save();

            var desde = new Date(2020, 10 - 1, 19).toISOString();
            var hasta = new Date(2020, 10 - 1, 21).toISOString();

            var aReserva = {
                desde,
                hasta,
                bicicleta: String(bicicleta._id),
                usuario: String(usuario._id),
            };
            request.post({
                    headers: headers,
                    url: `${base_url}/create`,
                    body: JSON.stringify(aReserva),
                },

                function (error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var reserva = JSON.parse(body);
                    expect(reserva.desde).toEqual(aReserva.desde);
                    expect(reserva.hasta).toEqual(aReserva.hasta);
                    expect(reserva.bicicleta).toEqual(aReserva.bicicleta);
                    expect(reserva.usuario).toEqual(aReserva.usuario);
                    done();
                }
            );
        });
    });

    describe("POST RESERVAS /update", () => {
        it("Status 200", (done) => {
            var headers = {
                "content-type": "application/json"
            };

            const usuario = new Usuario({
                nombre: "Jonathan"
            });
            usuario.save();

            const bicicleta = new Bicicleta({
                code: 1,
                color: "verde",
                modelo: "urbana",
            });
            bicicleta.save();

            var desde = new Date(2020, 10 - 1, 19).toISOString();
            var hasta = new Date(2020, 10 - 1, 21).toISOString();

            var aReserva = {
                desde,
                hasta,
                bicicleta: bicicleta._id,
                usuario: usuario._id,
            };
            Reserva.add(aReserva, function (err, newReserva) {
                newReserva.hasta = new Date(2020, 10 - 1, 23).toISOString();
                request.post({
                        headers: headers,
                        url: `${base_url}/update`,
                        body: JSON.stringify(newReserva),
                    },

                    function (error, response, body) {
                        expect(response.statusCode).toBe(200);
                        Reserva.findById(usuario.id, (err) => {
                            var reserva = JSON.parse(body);
                            if (err) console.log({
                                err
                            });
                            expect(reserva.hasta).toEqual(reserva.hasta);
                            done();
                        });
                    }
                );
            });
        });
    });

    describe("DELETE RESERVAS /delete", () => {
        it("Status 204", (done) => {
            const usuario = new Usuario({
                nombre: "Jonathan"
            });
            usuario.save();

            const bicicleta = new Bicicleta({
                code: 1,
                color: "verde",
                modelo: "urbana",
            });
            bicicleta.save();

            var desde = new Date(2020, 10 - 1, 19).toISOString();
            var hasta = new Date(2020, 10 - 1, 21).toISOString();

            var aReserva = {
                desde,
                hasta,
                bicicleta: String(bicicleta._id),
                usuario: String(usuario._id),
            };
            Reserva.add(aReserva, function (err, laReserva) {
                var headers = {
                    "content-type": "application/json"
                };
                const usuario2 = new Usuario({
                    nombre: "Matias"
                });
                usuario2.save();

                const bicicleta2 = new Bicicleta({
                    code: 2,
                    color: "azul",
                    modelo: "urbana",
                });
                bicicleta2.save();

                var desde = new Date(2020, 10 - 1, 20);
                var hasta = new Date(2020, 10 - 1, 21);

                var aReserva2 = new Reserva({
                    desde: desde,
                    hasta: hasta,
                    bicicleta: String(bicicleta2._id),
                    usuario: String(usuario2._id),
                });

                Reserva.add(aReserva2, function (err, otraReserva) {
                    request.delete({
                            headers: headers,
                            url: `${base_url}/delete`,
                            body: JSON.stringify(otraReserva),
                        },
                        function (error, response, body) {
                            expect(response.statusCode).toBe(204);
                            expect(Reserva.length).toBe(3);
                            done();
                        }
                    );
                });
            });
        });
    });
});
