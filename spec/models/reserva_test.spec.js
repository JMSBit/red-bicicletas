var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta");
var Usuario = require("../../models/usuario");
var Reserva = require("../../models/reserva");

describe("Testing Reserva", function () {
    beforeEach(function (done) {
        var mongoDB = "mongodb://localhost/testdb";
        mongoose.connect(mongoDB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });

        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "connection error"));
        db.once("open", function () {
            console.log("We are connected to test database");
            done();
        });
    });

    afterEach(function (done) {
        Usuario.deleteMany({}, function (err, success) {
            if (err) console.log(err);
            Bicicleta.deleteMany({}, function (err, success) {
                if (err) console.log(err);
                Reserva.deleteMany({}, function (err, success) {
                    if (err) console.log(err);
                    mongoose.disconnect(err);
                    done();
                });
            });
        });
    });

    describe("Reserva.add", () => {
        it("agrega una reserva", (done) => {
            const usuario = new Usuario({
                nombre: "Jonathan"
            });
            usuario.save();

            const bicicleta = new Bicicleta({
                code: 1,
                color: "verde",
                modelo: "urbana",
            });
            bicicleta.save();

            var desde = new Date(2020, 10 - 1, 18);
            var hasta = new Date(2020, 10 - 1, 21);

            var aReserva = new Reserva({
                desde: desde,
                hasta: hasta,
                bicicleta: bicicleta._id,
                usuario: usuario._id,
            });

            Reserva.add(aReserva, function (err, newReserva) {
                if (err) console.log("err");
                Reserva.find({}, function (err, reserva) {
                    expect(reserva.length).toEqual(1);

                    done();
                });
            });
        });
    });

    describe("Reserva.findById", () => {
        it("debe devolver la primer reserva", (done) => {
            Reserva.find({}, function (err, reservas) {
                expect(reservas.length).toBe(0);

                const usuario = new Usuario({
                    nombre: "Jonathan"
                });
                usuario.save();

                const bicicleta = new Bicicleta({
                    code: 1,
                    color: "verde",
                    modelo: "urbana",
                });
                bicicleta.save();

                var desde = new Date(2020, 10 - 1, 18);
                var hasta = new Date(2020, 10 - 1, 21);

                var aReserva = new Reserva({
                    desde: desde,
                    hasta: hasta,
                    bicicleta: bicicleta._id,
                    usuario: usuario._id,
                });
                Reserva.add(aReserva, function (err, newReserva) {
                    if (err) console.log("err");

                    const usuario2 = new Usuario({
                        nombre: "Matías"
                    });
                    usuario2.save();

                    const bicicleta2 = new Bicicleta({
                        code: 2,
                        color: "azul",
                        modelo: "urbana",
                    });
                    bicicleta2.save();

                    var desde = new Date(2020, 10 - 1, 20);
                    var hasta = new Date(2020, 10 - 1, 21);

                    var aReserva2 = new Reserva({
                        desde: desde,
                        hasta: hasta,
                        bicicleta: bicicleta2._id,
                        usuario: usuario2._id,
                    });
                    Reserva.add(aReserva2, function (err, newReserva) {
                        if (err) console.log("err");
                        Reserva.findById(usuario.id, function (error, targetReserva) {
                            expect(targetReserva.usuario).toEqual(aReserva.usuario._id);
                            expect(targetReserva.desde).toEqual(aReserva.desde);
                            expect(targetReserva.hasta).toEqual(aReserva.hasta);

                            done();
                        });
                    });
                });
            });
        });
    });

    describe("Reserva.removeById", () => {
        it("debe eliminar la primera reserva", (done) => {
            Reserva.find({}, function (err, reservas) {
                expect(reservas.length).toBe(0);

                const usuario = new Usuario({
                    nombre: "Jonathan"
                });
                usuario.save();

                const bicicleta = new Bicicleta({
                    code: 1,
                    color: "verde",
                    modelo: "urbana",
                });
                bicicleta.save();

                var desde = new Date(2020, 10 - 1, 18);
                var hasta = new Date(2020, 10 - 1, 21);

                var aReserva = new Reserva({
                    desde: desde,
                    hasta: hasta,
                    bicicleta: bicicleta._id,
                    usuario: usuario._id,
                });

                Reserva.add(aReserva, function (err, newReserva) {
                    if (err) console.log("err");

                    const usuario2 = new Usuario({
                        nombre: "Matías"
                    });
                    usuario2.save();

                    const bicicleta2 = new Bicicleta({
                        code: 2,
                        color: "azul",
                        modelo: "urbana",
                    });
                    bicicleta2.save();

                    var desde = new Date(2020, 10 - 1, 20);
                    var hasta = new Date(2020, 10 - 1, 21);

                    var aReserva2 = new Reserva({
                        desde: desde,
                        hasta: hasta,
                        bicicleta: bicicleta2._id,
                        usuario: usuario2._id,
                    });

                    Reserva.add(aReserva2, function (err, newReserva) {
                        if (err) console.log("err");
                        Reserva.removeById(usuario.id, function (error) {
                            Reserva.find({}, function (err, resv) {
                                expect(resv.length).toBe(1);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });
});